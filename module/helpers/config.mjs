export const NON = {};

/**
 * The set of Ability Scores used within the system.
 * @type {Object}
 */
NON.abilities = {
  str: 'NON.Ability.Str.long',
  dex: 'NON.Ability.Dex.long',
  con: 'NON.Ability.Con.long',
  int: 'NON.Ability.Int.long',
  wis: 'NON.Ability.Wis.long',
  cha: 'NON.Ability.Cha.long',
};

NON.abilityAbbreviations = {
  str: 'NON.Ability.Str.abbr',
  dex: 'NON.Ability.Dex.abbr',
  con: 'NON.Ability.Con.abbr',
  int: 'NON.Ability.Int.abbr',
  wis: 'NON.Ability.Wis.abbr',
  cha: 'NON.Ability.Cha.abbr',
};
