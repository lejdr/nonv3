import nonActorBase from "./actor-base.mjs";

export default class nonUnit extends nonActorBase {

  static defineSchema() {
    const fields = foundry.data.fields;
    const requiredInteger = { required: true, nullable: false, integer: true };
    const schema = super.defineSchema();


  schema.type = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.équipement = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.protection = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.qualité = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.moral = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.munitions = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.origine = new fields.SchemaField({
    valeur: new fields.NumberField({...requiredInteger, initial: 1 })
  });
  schema.puissance = new fields.NumberField({...requiredInteger, initial: 1});
  schema.portéeDeCommandement = new fields.NumberField({...requiredInteger, initial: 1});
  schema.disciplinée = new fields.BooleanField({...requiredInteger, initial: 1});
  schema.Unité = new fields.StringField({ required: true, blank: true});
  schema.SousUnité = new fields.StringField({ required: true, blank: true});
  return schema;
  }

  prepareDerivedData() {
    // Loop through ability scores, and add their modifiers to our sheet output.
    for (const key in this.abilities) {
      // Calculate the modifier using d20 rules.
      this.abilities[key].mod = Math.floor((this.abilities[key].value - 10) / 2);
      // Handle ability label localization.
      this.abilities[key].label = game.i18n.localize(CONFIG.NON.abilities[key]) ?? key;
    }
  }

  getRollData() {
    const data = {};

    // Copy the ability scores to the top level, so that rolls can use
    // formulas like `@str.mod + 4`.
    if (this.abilities) {
      for (let [k,v] of Object.entries(this.abilities)) {
        data[k] = foundry.utils.deepClone(v);
      }
    }

    data.lvl = this.attributes.level.value;

    return data
  }
}